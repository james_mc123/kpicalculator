// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
window.$ = window.jQuery = require("jQuery");

console.log($('render script started'));

var selectedValue;
var kpiCalculator = $('#kpiCalculator');

function generateForm() {
  var selectedKpi = selectedValue.innerHTML;
  selectedKpi = selectedKpi.replace(/\s/g, "");
  console.log ("selected kpi for form" + selectedKpi);
  var formVars = selectedKpi.split(/[\/\+\-\*]/g);
  var formOps = selectedKpi.match(/[\/\+\-\*]/g);

  console.log("array of variables for form: " + formVars);
  console.log("array of variables for form: " + formOps);

  kpiCalculator.children().remove();
  var kpiForm = kpiCalculator.show().append("<form></form>");
  kpiCalculator.append(kpiForm);
  for (var i = 0; i < formVars.length; i++) {
    console.log("rendering field for: " + formVars[i]);
    var formLabel = $('<b></b>');
    formLabel.attr('id', "lbl" + formVars[i]);
    formLabel.text(formVars[i]);

    var formTxt = $('<input>').attr('type', 'text');
    formTxt.attr('id', "txt" + formVars[i]);

    kpiForm.show().append(formLabel);
    kpiForm.show().append(formTxt);
    kpiForm.show().append("</br>");
  }

  var calcButton = $('<button>');
  calcButton.id = 'calcButton';
  calcButton.text("Calculate");
  calcButton.click(function(e) {

    for (var i = 0; i < formVars.length; i++) {
      var formLblId = '#lbl'+formVars[i];
      var formVarId = '#txt'+formVars[i];

      console.log("form label id " + formLblId);
      console.log("form label object" + $(formLblId));
      var formLblTxt = $(formLblId).text();

      console.log("form var id: " + formVarId);
      console.log("form var :" + $(formVarId));
      var formValue = $(formVarId).val();
      console.log("form value: " + formValue);
      console.log("label text: " + formLblTxt);
      selectedKpi = selectedKpi.replace(formLblTxt, formValue);
    }

    console.log(selectedKpi);

    var finalResult = eval(selectedKpi);
    console.log("final result: " + finalResult);
    var lblFinalResult = $('<b></b>');
    lblFinalResult.attr('id', 'lblFinalResult');
    lblFinalResult.text(finalResult)
    kpiCalculator.append('</br>');
    kpiCalculator.append(lblFinalResult);

    console.log("finished");
  });

  kpiCalculator.append(calcButton);
};

$('#btnAdd').click(function (){
  console.log("add clicked");
  var value = $('#inputFormula').val();
  console.log(value);

  if(value == "") {
    console.log("empty value");
    return;
  }

  var newListElement = $('<li></li>');
  newListElement.text(value);

  newListElement.click(function(e) {
    console.log("event" + e.target.textContent);
    console.log(e.target);
    selectedValue = e.target;
    $('#inputFormula').val(e.target.textContent);
    generateForm();
  });

  $('#formulaList').show().append(newListElement);
  $('#inputFormula').val("");
});


$('#btnEdit').click(function(e) {
  console.log("edit");
  console.log("editing " + selectedValue);
  var inputText = $('#inputFormula').val();
  selectedValue.innerHTML = inputText;
  $('#inputFormula').val("");
});


$('#btnRemove').click(function(e) {
  console.log("remove");
  selectedValue.remove();
  console.log(selectedValue);
  console.log($('#formulaList'));
  $('#inputFormula').val("");

});
